class User:
    def __init__(self, name, user_email, phone_number, current_job_title, password):
        self.name = name
        self.email = user_email
        self.phone_number = phone_number
        self.current_job_title = current_job_title
        self.password = password

    def change_password(self, new_password):
        self.password = new_password

    def change_job_title(self, change_job_title):
        self.current_job_title = change_job_title

    def get_user_info(self):
        print(f"User {self.name} currently works as a {self.current_job_title} and can be contacted on this line {self.phone_number} or email at {self.email}")