from user import User
from post import Post

app_user_one = User("Petr Obi", "president.elect@po", "+2349087654321", "President", "ewq,mn")
app_user_one.get_user_info()

app_user_two = User("Datti Ahmed", "vice-president.elect@po", "+2349087654345", "Vice-President", "wert/mn")
app_user_two.get_user_info()

new_post = Post("I and my boss are on a mission to put Nigeria back on track among top nations", app_user_two.name)
new_post.get_post_info()
